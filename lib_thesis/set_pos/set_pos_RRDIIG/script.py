#!/usr/bin/env python

import smach
import sys
import rospy
import time
import tf
import numpy as np
import matplotlib.pyplot as plt
import cv2
from sensor_msgs.msg import Image
from geometry_msgs.msg import PoseStamped 
from geometry_msgs.msg import Point
from std_msgs.msg import String
from cv_bridge import CvBridge, CvBridgeError
from mascor_px4_control_fks import MAV

def callback_loc_pose_1(poseStamped):	
	global current_z1, current_x1, current_y1
	current_x1 = poseStamped.pose.position.x
	current_y1 = poseStamped.pose.position.y   
	current_z1 = poseStamped.pose.position.z

def execute(self, inputs, outputs, gvm):
    x = inputs["x_pos"]
    y = inputs["y_pos"]
    z = inputs["z_pos"]

    # define UAV
    
    rospy.Subscriber('/uav1/mavros/local_position/pose', PoseStamped, callback_loc_pose_1)
    
    #gvm.get_variable("multicopter1",multicopter1,per_reference=True,default=None)
    
    #global multicopter1
    global stable_counter
    stable_counter = 0
    
    multicopter1 = MAV("/uav1", "multicopter")
    
    #multicopter1.goto(x, y, z,0)
    multicopter1.setpoint_pos([x, y, z])

    while not rospy.is_shutdown():
        print "x, y, z = %s, %s, %s" %(current_x1, current_y1, current_z1)
        if (abs(abs(current_x1)-abs(x)) < 0.1 and abs(abs(current_y1)-abs(y)) < 0.1 and abs(abs(current_z1)-abs(z)) < 0.1):
            stable_counter = stable_counter + 1
            print stable_counter
            if stable_counter > 100:
                return 0

