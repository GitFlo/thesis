#!/usr/bin/env python

# imports
import rospy
import sys
import time
import tf
import numpy as np
import cv2
from sensor_msgs.msg import Image
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Point
from std_msgs.msg import String
from cv_bridge import CvBridge, CvBridgeError
from mascor_px4_control import MAV

# initializing the node
rospy.init_node('uav_state_machine')
rate = rospy.Rate(25)
bridge = CvBridge()

# initializing the UAV
global multicopter
multicopter = MAV("multicopter")
dif = 0.15

def callback_loc_pose(poseStamped):
    global local_x, local_y, local_z
    local_x = poseStamped.pose.position.x - 10 # start point of uav is at x=-10, y=0, z=0
    local_y = poseStamped.pose.position.y
    local_z = poseStamped.pose.position.z

def start(x,y,z): # function for fly to position of piles and take photo
    stable_counter = 0
    global trans
    trans = 0

    # fly to location of the piles of boxes
    multicopter.setpoint_pos([x, y, z])

    while not rospy.is_shutdown():
        # checks if the UAV has the right position
        if local_x > x-10-dif and local_x < x-10+dif and local_y > y-dif and local_y < y+dif and local_z > z-dif and local_z < z+dif:
            stable_counter = stable_counter + 1
            # Wait until UAV stabilizes
            if stable_counter > 1000:
                print "successfully reached point x = %s, y = %s, z = %s" %(local_x, local_y, local_z)
                image_msg = rospy.wait_for_message('/camera_right/image_raw', Image)
                # function call for the image processing
                process_image(rospy.wait_for_message('/camera_right/image_raw', Image))
                time.sleep(10)
                trans = 1 # set transition to TRUE
                return 0

def fly_to(x, y, z): # function to fly to any position
    stable_counter = 0
    global trans
    trans = 0

    # fly to any location
    multicopter.setpoint_pos([x, y, z])

    while not rospy.is_shutdown():
        # checks if the UAV has the right position
        if local_x > x-10-dif and local_x < x-10+dif and local_y > y-dif and local_y < y+dif and local_z > z-dif and local_z < z+dif:
            stable_counter = stable_counter + 1
            # Wait until UAV stabilizes
            if stable_counter > 1000:                                                                                                        # Wait until drone stabilizes
                print "successfully reached point x = %s, y = %s, z = %s" %(local_x, local_y, local_z)
                trans = 1 # set transition to TRUE
                return 0

def main(x, y, z, x1, y1, z1): # inner level state machine
    global transb
    transb = 0

    # call of subscriber to get the local position of the UAV
    rospy.Subscriber('/mavros/local_position/pose', PoseStamped, callback_loc_pose)

    start(x,y,z) # calls function for arming and fly to position of piles

    if trans == 1:
        print "fly down to grasp object"
        fly_to(x, y, z1)
        if trans == 1:
            print "fly up again"
            fly_to(x, y, z)
            if trans == 1:
                print "fly to dropping zone"
                fly_to(x1, y1, z)
                if trans == 1:
                    print "fly down to drop object"
                    fly_to(x1, y1, z1)
                    if trans == 1:
                        print "object successfully dropped"
                        transb = 1
                        return 0

def process_image(img): # function for detecting the boxes

    try:
        # use of CcBridge to pass the camera topic to variable cv_image
        cv_image = bridge.imgmsg_to_cv2(img, "bgr8")
    except CvBridgeError as e:
        print(e)

    # Convert BGR to HSV (Hue, Saturation, Value)
    hsv = cv2.cvtColor(cv_image, cv2.COLOR_BGR2HSV)

    # define range of blue color in HSV
    lower_blue = np.array([110,50,50])
    upper_blue = np.array([130,255,255])

    # define range of green color in HSV
    lower_green = np.array([50,60,60])
    upper_green = np.array([70,255,255])

    # define range of red color in HSV
    lower_red = np.array([0,85,85])
    upper_red = np.array([10,255,255])

    # define range of orange color in HSV
    lower_orange = np.array([75,100,100])
    upper_orange = np.array([135,255,255])

    # Threshold the HSV image to get only blue colors
    blueMask = cv2.inRange(hsv, lower_blue, upper_blue)
    greenMask = cv2.inRange(hsv, lower_green, upper_green)
    redMask = cv2.inRange(hsv, lower_red, upper_red)
    orangeMask = cv2.inRange(hsv, lower_orange, upper_orange)

    # find contours and calculate the pixel coordinates of the box's center
    im2, contours_blue, hierarchy_blue = cv2.findContours(blueMask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    im2, contours_red, hierarchy_red = cv2.findContours(redMask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    im2, contours_green, hierarchy_green = cv2.findContours(greenMask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    im2, contours_orange, hierarchy_orange = cv2.findContours(orangeMask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # contour the objects
    cv2.drawContours(cv_image, contours_blue, -1,  (128, 0, 128), 3)
    cv2.drawContours(cv_image, contours_green, -1, (128, 0, 128), 3)
    cv2.drawContours(cv_image, contours_red, -1,   (128, 0, 128), 3)
    cv2.drawContours(cv_image, contours_orange, -1,   (128, 0, 128), 3)

    # resize format of the image to 35 percent
    cv_image = cv2.resize(cv_image, None, fx=0.35, fy=0.35, interpolation=cv2.INTER_CUBIC)

    # show the images
    cv2.imshow("Image window", cv_image)
    cv2.waitKey(3)
    return 0

if __name__ == '__main__': # first execution of the python code

    # outer level state machine

    # arming of the UAV
    multicopter.arm()

    # wait until UAV is ready to fly
    while not multicopter.setFlightmode("OFFBOARD"):
        rate.sleep()

    print "fly to first object"
    main(-10, -19.5, 10, 10, 0, 1) # calls inner level state machine
    if transb == 1:
        print "fly to second object"
        main(0, -19.5, 10, 12, 0, 1) # calls inner level state machine
        if transb == 1:
            print "fly to third object"
            main(10, -19.5, 10, 14, 0, 1) # calls inner level state machine
            if transb == 1:
                print "fly to fourth object"
                main(20, -19.5, 10, 16, 0, 1) # calls inner level state machine
