#!/usr/bin/env python

import smach
import sys
import rospy
import time
import tf
import numpy as np
import matplotlib.pyplot as plt
import cv2
from sensor_msgs.msg import Image
from geometry_msgs.msg import PoseStamped 
from geometry_msgs.msg import Point
from std_msgs.msg import String
from cv_bridge import CvBridge, CvBridgeError
from mascor_px4_control import MAV


def execute(self, inputs, outputs, gvm):
    uav_name = inputs["uav_name"]
    x = inputs["x_pos"]
    y = inputs["y_pos"]
    z = inputs["z_pos"]

    # define UAV
	global multicopter1
	global multicopter2
	multicopter1 = MAV("/uav1", "multicopter")
	multicopter2 = MAV("/uav2", "multicopter")
	multicopter1.arm()
	multicopter2.arm()

    rospy.wait_for_service(multicopter1)
	rospy.wait_for_service(multicopter1)
    self.logger.info("ROS external module: executed the arming")
	return 0







