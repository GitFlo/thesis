#!/usr/bin/env python
import sys
import rospy
import time
import tf
import numpy as np
import cv2
from cv_bridge import CvBridge, CvBridgeError
from mascor_px4_control import MAV
from geometry_msgs.msg import PoseStamped 
from sensor_msgs.msg import Image
from dronesim.srv import *
#from random import randint

poseStamped = PoseStamped()
coordinates_msg = []
toRemove= []
bridge = CvBridge()
image_msg = 0

global box_coordinates
box_coordinates = []
global sema_client
global manual_shutdown
global ii
ii=0
manual_shutdown = False
global local_x, local_y, local_z
local_x = 0
local_y = 0
local_z = 0

## setpoints for drone
sleeping = 10
dif = 0.5
x = 20
y = -17
z = 15
x1 = 20
y1 = -30
x2 = -25
y2 = -35
x3 = -25
y3 = -20

## init
rospy.init_node('provide_coords_server', anonymous=True)
rate = rospy.Rate(25)
listener = tf.TransformListener()
global multicopter2
multicopter2 = MAV("/uav2", "multicopter")

#cv_image = bridge.imgmsg_to_cv2(img, "bgr8")
def start(x, y, z):
    global multicopter2
    multicopter2.arm()

    while not multicopter2.setFlightmode("OFFBOARD"):
        rate.sleep()

    multicopter2.setpoint_pos([x, y, z])

def handle_provide_coords(req):
    global box_coordinates
    global sema_client
    global manual_shutdown
    global ii
    toSend = box_coordinates[ii]
    ii = ii+1
    if ii == len(box_coordinates)-1:
        manual_shutdown = True
    return ProvideCoordsResponse(toSend, len(box_coordinates))

def provide_coords_server():
    global manual_shutdown
    s = rospy.Service('provide_coords',ProvideCoords, handle_provide_coords)
    r = rospy.Rate(10)
    while not rospy.is_shutdown() and not manual_shutdown:
        r.sleep()
        print("manual_shutdown in while:  ")
        print(manual_shutdown)
    rospy.sleep(1)
    s.shutdown()    

def provide_destination_client():
    rospy.wait_for_service('provide_destination')
    try:
        provide_destination = rospy.ServiceProxy('provide_destination',ProvideDestination)
        resp = provide_destination()
        return resp
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e

def fly_to(x, y, z):
    global multicopter2
    multicopter2.setpoint_pos([x, y, z])

def process_image(img):
    coords = []
    try:
        cv_image = bridge.imgmsg_to_cv2(img, "bgr8")
    except CvBridgeError as e:
        print(e)
    
    # Convert BGR to HSV
    hsv = cv2.cvtColor(cv_image, cv2.COLOR_BGR2HSV)

    # define range of blue color in HSV
    lower_blue = np.array([110,50,50])
    upper_blue = np.array([130,255,255])
	
	# define range of green color in HSV
    lower_green = np.array([50,60,60])
    upper_green = np.array([70,255,255])
	
	# define range of red color in HSV
    lower_red = np.array([0,85,85])
    upper_red = np.array([10,255,255])
	
    # Threshold the HSV image to get only blue colors
    blueMask = cv2.inRange(hsv, lower_blue, upper_blue)
    greenMask = cv2.inRange(hsv, lower_green, upper_green)
    redMask = cv2.inRange(hsv, lower_red, upper_red)

	# add the masks to get one image with all boxes detected
    mask = cv2.add(blueMask, greenMask)
    mask = cv2.add(mask, redMask)
    
    # find contours and calculate the pixel coordinates of the box's center
    im2, contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
	
    for con in range(0, len(contours)):
        if cv2.contourArea(contours[con]) > 2000:
            continue
        M = cv2.moments(contours[con])
        cX = int(M['m10']/M['m00'])
        cY = int(M['m01']/M['m00'])
        coords.append([cX, cY])
        cv2.circle(cv_image, (cX, cY), 7, (0, 0, 0), -1)
    
    cv_image = cv2.resize(cv_image, None, fx=0.4, fy=0.4, interpolation=cv2.INTER_LINEAR)
    # show the images
    cv2.imshow("Image window2", cv_image)
    cv2.waitKey(3)

    return coords

def get_coordinates(cX, cY, local_x, local_y, local_z):				## possible error
    #local_x = poseStamped.pose.position.x
    #local_y = poseStamped.pose.position.y   
    #local_z = poseStamped.pose.position.z
	#transfer pixel coordinate into local coordinate
    #calculate local_x of package
    deltaX_pix = cX - 960
    deltaX_m = 2 * local_z * deltaX_pix / 1920
    
    #calculate local_y of package
    deltaY_pix = 540 - cY
    deltaY_m = (1920/1080) * local_z * deltaY_pix / 1080

    box_coords = [local_x + deltaX_m, local_y + deltaY_m]

    return box_coords

def callback_loc_pose(poseStamped):
    global local_x, local_y, local_z

    local_x = poseStamped.pose.position.x
    local_y = poseStamped.pose.position.y   
    local_z = poseStamped.pose.position.z

rospy.Subscriber('/uav2/mavros/local_position/pose', PoseStamped , callback_loc_pose)


if __name__ == '__main__':

    try:
        pixList= []			# list, which consits of lists of content [cX, cY]

       
        #drone flies to every given setpoint, waits there to stabilize and requests an image and coordinates
        start(x, y, z)
        while not rospy.is_shutdown():
            if local_x > x-dif and local_x < x+dif and local_y > y-dif and local_y < y+dif and local_z > z-dif and local_z < z+dif:
                time.sleep(sleeping)                                                                                                        # Wait until drone stabilizes
                print("pic 1")
                image_msg = rospy.wait_for_message('/uav2/camera_right/image_raw', Image)                                                   # Read in one image, to avoid getting image from previous pos      
                pixList.extend(process_image(rospy.wait_for_message('/uav2/camera_right/image_raw', Image)))                                # Write pixel coordinates to pixList
                while len(coordinates_msg) < len(pixList):                                                                                  # write drones coordinates to list
                    coordinates_msg.append([local_x, local_y, local_z])                                                                         #loop until both lists are same size
                #print(coordinates_msg)
                fly_to(x1, y1, z)                                                                                                           # Fly to next point
                time.sleep(2)                                                                                                               # wait, to not take the same picture another time                                                                                                
             
            if local_x > x1-dif and local_x < x1+dif and local_y > y1-dif and local_y < y1+dif and local_z > z-dif and local_z < z+dif:
                time.sleep(sleeping)
                print("pic 2")
                image_msg = rospy.wait_for_message('/uav2/camera_right/image_raw', Image)
                pixList.extend(process_image(rospy.wait_for_message('/uav2/camera_right/image_raw', Image)))
                while len(coordinates_msg) < len(pixList):
                    coordinates_msg.append([local_x, local_y, local_z])
                fly_to(x2, y2, z)
                time.sleep(2)            
            if local_x > x2-dif and local_x < x2+dif and local_y > y2-dif and local_y < y2+dif and local_z > z-dif and local_z < z+dif:
                time.sleep(sleeping)
                print("pic 3")
                image_msg = rospy.wait_for_message('/uav2/camera_right/image_raw', Image)
                pixList.extend(process_image(rospy.wait_for_message('/uav2/camera_right/image_raw', Image)))
                while len(coordinates_msg) < len(pixList):
                    coordinates_msg.append([local_x, local_y, local_z])
                fly_to(x3, y3, z)
                time.sleep(2)
            if local_x > x3-dif and local_x < x3+dif and local_y > y3-dif and local_y < y3+dif and local_z > z-dif and local_z < z+dif:
                time.sleep(sleeping)
                print("pic 4")
                image_msg = rospy.wait_for_message('/uav2/camera_right/image_raw', Image)
                pixList.extend(process_image(rospy.wait_for_message('/uav2/camera_right/image_raw', Image)))
                while len(coordinates_msg) < len(pixList):
                    coordinates_msg.append([local_x, local_y, local_z])
                time.sleep(2)
                break
            

		#calculate the real world box coordinates from every image
        for i in range(0, len(coordinates_msg)):
            box_coordinates.append(get_coordinates(pixList[i][0], pixList[i][1], coordinates_msg[i][0], coordinates_msg[i][1], coordinates_msg[i][2]))

        #print("length of box list (before):  ")
        #print(len(box_coordinates))
        
		#look for coordinates which are saved twice and delete them
        for i in range(0, len(box_coordinates)-1):
            for x in range(i+1, len(box_coordinates)):
                if box_coordinates[i][0] < box_coordinates[x][0]+2 and box_coordinates[i][0] > box_coordinates[x][0]-2 and box_coordinates[i][1] < box_coordinates[x][1]+2 and box_coordinates[i][1] > box_coordinates[x][1]-2:
                    box_coordinates[x]=[0,0]              #continue here
        box_coordinates=list(filter(lambda x: x != [0, 0], box_coordinates))
        
        #print("length of box list (after):  ")
        #print(len(box_coordinates))
        #print(box_coordinates)

        provide_coords_server()
        
        z = 2
 
        while not rospy.is_shutdown():
            new_coords = provide_destination_client()
            new_coords.dest_x = new_coords.dest_x
            fly_to(new_coords.dest_x, new_coords.dest_y, z)
            while not (local_x > new_coords.dest_x-dif and local_x < new_coords.dest_x+dif and local_y > new_coords.dest_y-dif and local_y < new_coords.dest_y+dif and local_z > z-dif and local_z < z+dif):
                rate.sleep()
                #print("stuck in while")
            time.sleep(10)

    except rospy.ROSInterruptException:
        sys.exit() 










