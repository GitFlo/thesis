#!/usr/bin/env python

import sys
import rospy
import numpy as np
from dronesim.srv import *
global box_coordinates
box_coordinates = []

rospy.init_node('provide_destination_server')

def provide_coords_client():
    rospy.wait_for_service('provide_coords')
    try:
        provide_coords = rospy.ServiceProxy('provide_coords',ProvideCoords)
        resp = provide_coords()
        return resp
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e

def handle_provide_destination(req):
    global box_coordinates
    rand = np.random.randint(0,len(box_coordinates)-1)
    send_coords = box_coordinates[rand]
    del box_coordinates[rand]
    #print(send_coords)
    return ProvideDestinationResponse(send_coords[0], send_coords[1])

def provide_destination_server():
    ser = rospy.Service('provide_destination',ProvideDestination, handle_provide_destination)
    rospy.spin()

if __name__ == "__main__":
    try:
        vec=provide_coords_client()
        box_coordinates.append(vec.coords)
        for i in range(1, vec.length):
            vec = provide_coords_client()
            box_coordinates.append(vec.coords)
        #print(box_coordinates)


        provide_destination_server()

    except rospy.ROSInterruptException:
        sys.exit()
    
