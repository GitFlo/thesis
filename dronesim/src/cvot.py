#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cv2
import numpy as np
from matplotlib import pyplot as plt

#cap = cv2.VideoCapture(0)

#while(1):

#    # Take each frame
#    _, frame = cap.read()

#    # Convert BGR to HSV
#    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

#    # define range of blue color in HSV
#    lower_blue = np.array([110,50,50])
#    upper_blue = np.array([130,255,255])

#    # Threshold the HSV image to get only blue colors
#    mask = cv2.inRange(hsv, lower_blue, upper_blue)

#    # Bitwise-AND mask and original image
#    res = cv2.bitwise_and(frame,frame, mask= mask)

#    cv2.imshow('frame',frame)
#    cv2.imshow('mask',mask)
#    cv2.imshow('res',res)
#    k = cv2.waitKey(5) & 0xFF
#    if k == 27:
#        break

#cv2.destroyAllWindows()

green = np.uint8([[[255,137,13]]])
hsv_green = cv2.cvtColor(green,cv2.COLOR_BGR2HSV)
print hsv_green

#img = cv2.imread('gradient.png',0)
#ret,thresh1 = cv2.threshold(img,127,255,cv2.THRESH_BINARY)
#ret,thresh2 = cv2.threshold(img,127,255,cv2.THRESH_BINARY_INV)
#ret,thresh3 = cv2.threshold(img,127,255,cv2.THRESH_TRUNC)
#ret,thresh4 = cv2.threshold(img,127,255,cv2.THRESH_TOZERO)
#ret,thresh5 = cv2.threshold(img,127,255,cv2.THRESH_TOZERO_INV)

#titles = ['Original Image','BINARY','BINARY_INV','TRUNC','TOZERO','TOZERO_INV']
#images = [img, thresh1, thresh2, thresh3, thresh4, thresh5]

#for i in xrange(6):
#    plt.subplot(2,3,i+1),plt.imshow(images[i],'gray')
#    plt.title(titles[i])
#    plt.xticks([]),plt.yticks([])

#plt.show()


