#!/usr/bin/env python

import rospy
import smach
import smach_ros
import sys
import time
import tf
import numpy as np
import matplotlib.pyplot as plt
import cv2
from sensor_msgs.msg import Image
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Point
from std_msgs.msg import String
from cv_bridge import CvBridge, CvBridgeError
from mascor_px4_control import MAV
from mascor_px4_control import MAV2


def callback_loc_pose_1(poseStamped):
        global current_z1, current_x1, current_y1
        current_x1 = poseStamped.pose.position.x
        current_y1 = poseStamped.pose.position.y
        current_z1 = poseStamped.pose.position.z

def callback_loc_pose_2(poseStamped):
        global current_z2, current_x2, current_y2
        current_x2 = poseStamped.pose.position.x
        current_y2 = poseStamped.pose.position.y
        current_z2 = poseStamped.pose.position.z

def callback_cam_uav1(img):
        global height, width

# define state Foo
class Foo(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['outcome1','outcome2'])
        #self.counter = 0

    def execute(self, userdata):
        rospy.loginfo('Executing state FOO')

        #multicopter1 = MAV2("multicopter")
        #multicopter1.takeoff(3)
        return 'outcome1'
        #if self.counter < 3:
        #    self.counter += 1
        #    return 'outcome1'
        #else:
        #    return 'outcome2'


# define state Bar
class Bar(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['outcome1'])

    def execute(self, userdata):
        rospy.loginfo('Executing state BAR')

        global current_x, current_y, current_z
        global stable_counter
        stable_counter = 0
        local_target = Point()

        # Give init point
        local_target.x = 0
        local_target.y = 0
        local_target.z = 3

        #multicopter1 = MAV("multicopter")
        global multicopter1
        multicopter1.setpoint_pos([local_target.x, local_target.y, local_target.z])
        while not rospy.is_shutdown():
                print "x, y, z = %s, %s, %s" %(current_x1, current_y1, current_z1)
                if (abs(abs(current_x1)-abs(local_target.x)) < 0.1 and abs(abs(current_y1)-abs(local_target.y)) < 0.1 and abs(abs(current_z1)-abs(local_target.z)) < 0.1):
                    stable_counter = stable_counter + 1
                    print stable_counter
                    if stable_counter > 1000:
                        return 'outcome1'
        


# define state Arm
class Arm(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['outcome3'])

    def execute(self, userdata):
        rospy.loginfo('Executing state ARM')
        multicopter1 = MAV("multicopter")
        multicopter1.arm()
        return 'outcome3'




def main():
    rospy.init_node('uav_state_machine')

    rospy.Subscriber('/mavros/local_position/pose', PoseStamped, callback_loc_pose_1)
    #rospy.Subscriber('/uav1/camera/image_raw', Image , callback_cam_uav1)

    rate = rospy.Rate(25)
    listener = tf.TransformListener()

    # define UAV
    global multicopter1



    # Create the top level SMACH state machine
    sm_top = smach.StateMachine(outcomes=['outcome5'])
    
    # Open the container
    with sm_top:

        smach.StateMachine.add('ARM', Arm(),
                               transitions={'outcome3':'FLY'})

        # Create the sub SMACH state machine
        sm_sub = smach.StateMachine(outcomes=['outcome4'])

        # Open the container
        with sm_sub:

            # Add states to the container
            smach.StateMachine.add('FOO', Foo(), 
                                   transitions={'outcome1':'BAR', 
                                                'outcome2':'outcome4'})
            smach.StateMachine.add('BAR', Bar(), 
                                   transitions={'outcome1':'FOO'})

        smach.StateMachine.add('FLY', sm_sub,
                               transitions={'outcome4':'outcome5'})

    # Execute SMACH plan
    outcome = sm_top.execute()



if __name__ == '__main__':
    main()
