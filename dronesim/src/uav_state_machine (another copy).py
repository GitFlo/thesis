#!/usr/bin/env python

import rospy
import smach
import smach_ros
import sys
import time
import tf
import numpy as np
import matplotlib.pyplot as plt
import cv2
from sensor_msgs.msg import Image
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Point
from std_msgs.msg import String
from cv_bridge import CvBridge, CvBridgeError
from mascor_px4_control import MAV

rospy.init_node('uav_state_machine')

rate = rospy.Rate(25)
listener = tf.TransformListener()
bridge = CvBridge()

## setpoints for drone
dif = 0.15
x = -20 + 10 # start point of uav is at x=-10, y=0, z=0
y = -18.5
z = 10
x1 = 0 +10 # start point of uav is at x=-10, y=0, z=0
y1 = 0
z1 = 1
x2 = -25 +10 # start point of uav is at x=-10, y=0, z=0
y2 = -35
x3 = -25 +10 # start point of uav is at x=-10, y=0, z=0
y3 = -20

global multicopter
multicopter = MAV("multicopter")

def callback_loc_pose(poseStamped):
    global local_x, local_y, local_z
    local_x = poseStamped.pose.position.x - 10 # start point of uav is at x=-10, y=0, z=0
    local_y = poseStamped.pose.position.y
    local_z = poseStamped.pose.position.z

#def callback_img(img):
#    try:
#        cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
#    except CvBridgeError as e:
#        print(e)

def start(x,y,z):
    stable_counter = 0
    global trans
    trans = 0
    global multicopter
    multicopter.arm()

    while not multicopter.setFlightmode("OFFBOARD"):
        rate.sleep()

    multicopter.setpoint_pos([x, y, z])

    while not rospy.is_shutdown():
        #print "x, y, z = %s, %s, %s" %(local_x, local_y, local_z)
        if local_x > x-10-dif and local_x < x-10+dif and local_y > y-dif and local_y < y+dif and local_z > z-dif and local_z < z+dif:
            stable_counter = stable_counter + 1
            #print stable_counter
            if stable_counter > 1000:                                                                                                        # Wait until drone stabilizes
                print "successfully reached start point x = %s, y = %s, z = %s" %(local_x, local_y, local_z)

                image_msg = rospy.wait_for_message('/camera_right/image_raw', Image)
                image_msg2 = rospy.wait_for_message('/camera_right/image_raw', Image)
                process_image(rospy.wait_for_message('/camera_right/image_raw', Image))
                time.sleep(10)
                trans = 1
                return 0


def fly_to(x, y, z):
    stable_counter = 0
    global trans
    trans = 0
    global multicopter

    multicopter.setpoint_pos([x, y, z])

    while not rospy.is_shutdown():
        #print "x, y, z = %s, %s, %s" %(local_x, local_y, local_z)
        if local_x > x-10-dif and local_x < x-10+dif and local_y > y-dif and local_y < y+dif and local_z > z-dif and local_z < z+dif:
            stable_counter = stable_counter + 1
            #print stable_counter
            if stable_counter > 1000:                                                                                                        # Wait until drone stabilizes
                print "successfully reached point x = %s, y = %s, z = %s" %(local_x, local_y, local_z)
                trans = 1
                return 0

def main(x, y, z, x1, y1, z1):
    global transb
    transb = 0

    rospy.Subscriber('/mavros/local_position/pose', PoseStamped, callback_loc_pose)

    #rospy.Subscriber('/uav1/camera/image_raw', Image , callback_cam_uav1)
#    cv2.destroyAllWindows()
    start(x,y,z)

    if trans == 1:
#        image_msg = 0
#        image_msg = rospy.wait_for_message('/camera_right/image_raw', Image)
#        process_image()

#        image_sub = rospy.Subscriber('/camera_right/image_raw',Image,process_image)

        print "fly down to grasp object"
        fly_to(x, y, z1)
        if trans == 1:
            print "fly up again"
            fly_to(x, y, z)
            if trans == 1:
                print "fly to dropping zone"
                fly_to(x1, y1, z)
                if trans == 1:
                    print "fly down to drop object"
                    fly_to(x1, y1, z1)
                    if trans == 1:
                        print "object successfully dropped"
                        transb = 1
                        return 0

def process_image(img):


    try:
#        image_msg = rospy.wait_for_message('/camera_right/image_raw', Image)
#        rospy.loginfo("Got image!")
        cv_image = bridge.imgmsg_to_cv2(img, "bgr8")
    except CvBridgeError as e:
        print(e)

#    cv_image = cv2.resize(cv_image, None, fx=0.4, fy=0.4, interpolation=cv2.INTER_LINEAR)
#    # show the images
#    cv2.imshow("Image window", cv_image)
#    cv2.waitKey(3)

    # Convert BGR to HSV
    hsv = cv2.cvtColor(cv_image, cv2.COLOR_BGR2HSV)

    # define range of blue color in HSV
    lower_blue = np.array([110,50,50])
    upper_blue = np.array([130,255,255])

    # define range of green color in HSV
    lower_green = np.array([50,60,60])
    upper_green = np.array([70,255,255])

    # define range of red color in HSV
    lower_red = np.array([0,85,85])
    upper_red = np.array([10,255,255])

    # define range of orange color in HSV
    lower_orange = np.array([75,100,100])
    upper_orange = np.array([135,255,255])

    # Threshold the HSV image to get only blue colors
    blueMask = cv2.inRange(hsv, lower_blue, upper_blue)
    greenMask = cv2.inRange(hsv, lower_green, upper_green)
    redMask = cv2.inRange(hsv, lower_red, upper_red)
    orangeMask = cv2.inRange(hsv, lower_orange, upper_orange)

    # find contours and calculate the pixel coordinates of the box's center
    im2, contours_blue, hierarchy_blue = cv2.findContours(blueMask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    im2, contours_red, hierarchy_red = cv2.findContours(redMask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    im2, contours_green, hierarchy_green = cv2.findContours(greenMask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    im2, contours_orange, hierarchy_orange = cv2.findContours(orangeMask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # contour the objects
    cv2.drawContours(cv_image, contours_blue, -1,  (128, 0, 128), 3)
    cv2.drawContours(cv_image, contours_green, -1, (128, 0, 128), 3)
    cv2.drawContours(cv_image, contours_red, -1,   (128, 0, 128), 3)
    cv2.drawContours(cv_image, contours_orange, -1,   (128, 0, 128), 3)

#        # add the masks to get one image with all boxes detected
#    mask = cv2.add(blueMask, greenMask)
#    mask = cv2.add(mask, redMask)
#    mask = cv2.add(mask, orangeMask)

#    # find contours and calculate the pixel coordinates of the box's center
#    im2, contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

#    for con in range(0, len(contours)):
#        if cv2.contourArea(contours[con]) > 2000:
#            continue
#        M = cv2.moments(contours[con])
#        cX = int(M['m10']/M['m00'])
#        cY = int(M['m01']/M['m00'])
#        cv2.circle(cv_image, (cX, cY), 7, (0, 0, 0), -1)


    cv_image = cv2.resize(cv_image, None, fx=0.35, fy=0.35, interpolation=cv2.INTER_CUBIC)
    # show the images

    cv2.imshow("Image window", cv_image)
    cv2.waitKey(3)
    return 0

if __name__ == '__main__':
    print "fly to first object"
    main(-10, -18.5, 10, 10, 0, 1)
    if transb == 1:
        print "fly to second object"
        main(0, -18.5, 10, 12, 0, 1)
        if transb == 1:
            print "fly to third object"
            main(10, -18.5, 10, 14, 0, 1)
            if transb == 1:
                print "fly to fourth object"
                main(20, -18.5, 10, 16, 0, 1)
