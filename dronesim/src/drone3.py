#!/usr/bin/env python
import sys
import rospy
import time
#import tf
import numpy as np
#import cv2
#from cv_bridge import CvBridge, CvBridgeError
from mascor_px4_control import MAV
from geometry_msgs.msg import PoseStamped 
#from sensor_msgs.msg import Image
from dronesim.srv import *
#from random import randint


z = 1.5
dif = .5

global local_x, local_y, local_z
rospy.init_node('drone3', anonymous=True)
rate = rospy.Rate(25)
global multicopter3
multicopter3 = MAV("/uav3", "multicopter")

def fly_to(x, y, z):
    global multicopter3
    multicopter3.setpoint_pos([x, y, z])

def provide_destination_client():
    rospy.wait_for_service('provide_destination')
    try:
        provide_destination = rospy.ServiceProxy('provide_destination',ProvideDestination)
        resp = provide_destination()
        return resp
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e

def callback_loc_pose(poseStamped):
    global local_x, local_y, local_z

    local_x = poseStamped.pose.position.x
    local_y = poseStamped.pose.position.y   
    local_z = poseStamped.pose.position.z


rospy.Subscriber('/uav3/mavros/local_position/pose', PoseStamped , callback_loc_pose)

if __name__ == "__main__":
    
    try:
        while not rospy.is_shutdown():
            new_coords = provide_destination_client()
            new_coords.dest_x = new_coords.dest_x - 2.5
            fly_to(new_coords.dest_x, new_coords.dest_y, z)
            while not (local_x > new_coords.dest_x-dif and local_x < new_coords.dest_x+dif and local_y > new_coords.dest_y-dif and local_y < new_coords.dest_y+dif and local_z > z-dif and local_z < z+dif):
                rate.sleep()
                #print("stuck in while")
            time.sleep(10)
        
    except rospy.ROSInterruptException:
        sys.exit() 
