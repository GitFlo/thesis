#!/usr/bin/env python

import smach
import sys
import rospy
import time
import tf
import numpy as np
import matplotlib.pyplot as plt
import cv2
from sensor_msgs.msg import Image
from geometry_msgs.msg import PoseStamped 
from geometry_msgs.msg import Point
from std_msgs.msg import String
from cv_bridge import CvBridge, CvBridgeError
from mascor_px4_control import MAV


# create MAV object

def callback_loc_pose_1(poseStamped):	
	global current_z1, current_x1, current_y1
	current_x1 = poseStamped.pose.position.x
	current_y1 = poseStamped.pose.position.y   
	current_z1 = poseStamped.pose.position.z

def callback_loc_pose_2(poseStamped):	
	global current_z2, current_x2, current_y2
	current_x2 = poseStamped.pose.position.x
	current_y2 = poseStamped.pose.position.y   
	current_z2 = poseStamped.pose.position.z

def callback_cam_uav1(img):
	global height, width
	

if __name__ == '__main__':
	# define Subscribers 

        rospy.Subscriber('/mavros/local_position/pose', PoseStamped, callback_loc_pose_1)
        #rospy.Subscriber('/uav2/mavros/local_position/pose', PoseStamped, callback_loc_pose_2)
        #rospy.Subscriber('/uav1/camera/image_raw', Image , callback_cam_uav1)

	# initial node
	rospy.init_node('SynchronousUAV', anonymous=True)
	rate = rospy.Rate(25)
	listener = tf.TransformListener()
	
	# define UAV
	global multicopter1
	global multicopter2
	multicopter1 = MAV("/uav1", "multicopter")
	#multicopter2 = MAV("/uav2", "multicopter")
	multicopter1.arm()
	#multicopter2.arm()

	global current_x, current_y, current_z
        global stable_counter
        stable_counter = 0
	
	given_target = Point()
	local_target = Point()
        	
	# Give init point
        local_target.x = 0
        local_target.y = 0
        local_target.z = 3
        
        #multicopter1.goto(0.0, 0.0, 5.0, 0.0)
        multicopter1.setpoint_pos([local_target.x, local_target.y, local_target.z])

        while not rospy.is_shutdown():
                print "x, y, z = %s, %s, %s" %(current_x1, current_y1, current_z1)
                if (abs(abs(current_x1)-abs(local_target.x)) < 0.1 and abs(abs(current_y1)-abs(local_target.y)) < 0.1 and abs(abs(current_z1)-abs(local_target.z)) < 0.1):
                    stable_counter = stable_counter + 1
                    print stable_counter
                    if stable_counter > 1000:
                        local_target.x = 0
                        local_target.y = -2
                        local_target.z = 3
                        multicopter1.setpoint_pos([local_target.x, local_target.y, local_target.z])

                        while not rospy.is_shutdown():
                                print "x, y, z = %s, %s, %s" %(current_x1, current_y1, current_z1)
                                if (abs(abs(current_x1)-abs(local_target.x)) < 0.1 and abs(abs(current_y1)-abs(local_target.y)) < 0.1 and abs(abs(current_z1)-abs(local_target.z)) < 0.1):
                                    stable_counter = stable_counter + 1
                                    print stable_counter
                                    if stable_counter > 1000:
                                        local_target.x = 2
                                        local_target.y = -2
                                        local_target.z = 3
                                        multicopter1.setpoint_pos([local_target.x, local_target.y, local_target.z])

                                        while not rospy.is_shutdown():
                                                print "x, y, z = %s, %s, %s" %(current_x1, current_y1, current_z1)
                                                if (abs(abs(current_x1)-abs(local_target.x)) < 0.1 and abs(abs(current_y1)-abs(local_target.y)) < 0.1 and abs(abs(current_z1)-abs(local_target.z)) < 0.1):
                                                    stable_counter = stable_counter + 1
                                                    print stable_counter
                                                    if stable_counter > 1000:
                                                        local_target.x = 2
                                                        local_target.y = -10
                                                        local_target.z = 3
                                                        multicopter1.setpoint_pos([local_target.x, local_target.y, local_target.z])

                                                        while not rospy.is_shutdown():
                                                                print "x, y, z = %s, %s, %s" %(current_x1, current_y1, current_z1)
                                                                if (abs(abs(current_x1)-abs(local_target.x)) < 0.1 and abs(abs(current_y1)-abs(local_target.y)) < 0.1 and abs(abs(current_z1)-abs(local_target.z)) < 0.1):
                                                                    stable_counter = stable_counter + 1
                                                                    print stable_counter
                                                                    if stable_counter > 1000:
                                                                        rate.sleep()

