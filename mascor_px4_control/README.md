# mascor_px4_control library
## author
Tobias Müller, B.Eng. - University of Applied Sciences Aachen
mail: tobias.mueller1@alumni.fh-aachen.de
## info
This libray should simplify the control of the PX4 stack.

## included
- library (python)
- library testscript (python)

## functions
- safety zone
- arm/disarm
- takeoff/land
- return to land
- go to position
- position queue
- velocity control (vx,vy,vz)/(vx,vy,z)

## howto

- simply import the library into your script with

```
from mascor_px4_control import MAV
```

- have a look into lib_test.py for examples

## todo (WIP)
- LICENCES file
- yaw to next position
- sequencer
- better multithreading

## roadmap
- handle px4 new offboard plans
- c++ implementation
