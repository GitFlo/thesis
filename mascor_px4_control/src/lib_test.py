#!/usr/bin/env python
import sys
import rospy
import time
import tf
from mascor_px4_control import MAV


def run_tests():
    rospy.init_node('mascorPX4controlTest', anonymous=True)
    rate = rospy.Rate(25)
    listener = tf.TransformListener()
    ## create MAV object
    multicopter1 = MAV("", "multicopter")
    '''multicopter2 = MAV("/uav2", "multicopter")
    multicopter3 = MAV("/uav3", "multicopter")
    multicopter4 = MAV("/uav4", "multicopter")
    
    ## fence check with RTL
    multicopter1.setSafetyZone(-5,-5,0,5,5,5)
    while not rospy.is_shutdown():
        trans, rot = listener.lookupTransform("controller1", "world",rospy.Time.now())
        multicopter1.setpoint_pos(trans)
    '''
    '''for i in range(0, 20, 1):
        multicopter1.setpoint_vel(0.5 + i/10,0,0,4,0)
        rate.sleep()
    ## check if landed after RTL
    while not multicopter1.landed():
        rate.sleep()'''
    #multicopter1.setSafetyZone(-20,-20,0,20,20,20)
    setpoints = [[-3,0,3], [-3,0,9], [3,0,9], [3,0,3]]
    multicopter1.arm()
    #multicopter2.arm()
    #multicopter3.arm()
    #multicopter4.arm()
    while not multicopter1.setFlightmode("OFFBOARD"):
        rate.sleep()
    while not multicopter2.setFlightmode("OFFBOARD"):
        rate.sleep()
    while not multicopter3.setFlightmode("OFFBOARD"):
        rate.sleep()
    while not multicopter4.setFlightmode("OFFBOARD"):
        rate.sleep()
    multicopter1.setpoint_pos([0,0,3])
    #multicopter2.setpoint_pos([0,0,3])
    #multicopter3.setpoint_pos([0,0,3])
    #multicopter4.setpoint_pos([0,0,3])
    while not rospy.is_shutdown():
        for i in range(0, len(setpoints), 1):
            while True:
                if multicopter1.reachedGoto() and multicopter2.reachedGoto() and multicopter3.reachedGoto() and multicopter4.reachedGoto():
                    multicopter1.setpoint_pos(setpoints[i+1])
                    #multicopter2.setpoint_pos(setpoints[i])
                    #multicopter3.setpoint_pos(setpoints[i])
                    #multicopter4.setpoint_pos(setpoints[i])
                    break
                rate.sleep()

    '''
    while not multicopter1.takeoff(1.5):
        rate.sleep()
    while not multicopter2.takeoff(1.5):
        rate.sleep()
    while not multicopter1.setFlightmode("OFFBOARD"):
        rate.sleep()
    while not multicopter2.setFlightmode("OFFBOARD"):
        rate.sleep()
    rate.sleep()
    ## draw house five times
    for i in range(1,5):
        
        multicopter1.clearQueue()
        multicopter1.appendToQueue(6,0,2.5,0.5)
        multicopter1.appendToQueue(6,6,2.5,0.5)
        multicopter1.appendToQueue(0,6,2.5,0.5)
        multicopter1.appendToQueue(0,0,2.5,0.5)
        multicopter1.startQueue()
        multicopter2.clearQueue()
        multicopter2.appendToQueue(6,0,2.5,0.5)
        multicopter2.appendToQueue(6,6,2.5,0.5)
        multicopter2.appendToQueue(0,6,2.5,0.5)
        multicopter2.appendToQueue(0,0,2.5,0.5)
        multicopter2.startQueue()
        multicopter1.goto(3,-3,2.5, 0.5)
        while not multicopter1.reachedGoto():
            rate.sleep()
        multicopter1.goto(6,0,2.5, 0.5)
        while not multicopter1.reachedGoto():
            rate.sleep()
        multicopter1.goto(0,0,2.5, 0.5)
        while not multicopter1.reachedGoto():
            rate.sleep() 
    
    ## RTL
    multicopter1.returnToHome()
    multicopter2.returnToHome()
    ## delete object
    while not multicopter1.landed():
        rate.sleep()
    while not multicopter2.landed():
        rate.sleep()
    while not multicopter1.delete():
        rate.sleep()
    while not multicopter2.delete():
        rate.sleep()

    ## wait for killed threads
    time.sleep(1) 
    '''
if __name__ == '__main__':

    try:
        run_tests()
    except rospy.ROSInterruptException:
        sys.exit() 
